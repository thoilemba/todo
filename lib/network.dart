import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';


// TODO: Class to work with API requests (CRUD operation)

class NetworkHelper {

  static const localhostAPI = 'http://127.0.0.1:3001/api/v1/todos/';
  static const emulatorAPI  = 'http://10.0.2.2:3001/api/v1/todos/';
  static const realDeviceAPI = 'http://192.168.1.4:3001/api/v1/todos/';

  static Future<dynamic> fetchData() async {
    String url = realDeviceAPI;
    final response = await http.get(Uri.parse(url));

    if(response.statusCode == 200){
      final taskData = json.decode(response.body);
      return taskData;
    }else{
      showMessage('Failed to load tasks. Error code: ${response.statusCode}', Colors.redAccent);
    }
  }

  static Future<void> postData(String title) async {

    DateTime timestamp = DateTime.now();
    String formattedTimestamp = DateFormat('yyyy-MM-dd HH:mm:ss').format(timestamp);

    final response = await http.post(
      Uri.parse(realDeviceAPI),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        // 'id': 5,
        'title': title,
        'completed': false,
        'created': formattedTimestamp,
      }),
    );

    if(response.statusCode == 201) {
      showMessage('Task Added', Colors.greenAccent);
    }else{
      showMessage('Failed to post data. Error code: ${response.statusCode}', Colors.redAccent);
    }
  }

  static Future<void> checkBoxUpdate(bool? value, int id, String title) async {

    DateTime timestamp = DateTime.now();
    String formattedTimestamp = DateFormat('yyyy-MM-dd HH:mm:ss').format(timestamp);

    final response = await http.put(
      Uri.parse('$realDeviceAPI$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'id': id,
        'title': title,
        'completed': value,
        'created': formattedTimestamp,
      }),
    );

    if(response.statusCode == 201){
      showMessage('Task Updated', Colors.greenAccent);
    }else {
      showMessage('Failed to update task. Error code: ${response.statusCode}', Colors.redAccent);
    }
  }

  static Future<void> update(bool? value, int id, String title) async {
    DateTime timestamp = DateTime.now();
    String formattedTimestamp = DateFormat('yyyy-MM-dd HH:mm:ss').format(timestamp);

    final response = await http.put(
      Uri.parse('$realDeviceAPI$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'id': id,
        'title': title,
        'completed': value,
        'created': formattedTimestamp,
      }),
    );

    if(response.statusCode == 201){
      showMessage('Task Edited', Colors.greenAccent);
    }else {
      showMessage('Failed to update task. Error code: ${response.statusCode}', Colors.redAccent);
    }
  }

  static Future<void> deleteByID(BuildContext context, int id) async {
    String url = '$realDeviceAPI$id';
    final response = await http.delete(Uri.parse(url));

    if(response.statusCode == 200){
      // showMessage('Task deleted', Colors.greenAccent);
      showSuccessMessage(context, "Task Deleted");
    }else {
      showMessage('Failed to delete task. Error code: ${response.statusCode}', Colors.redAccent);
    }
  }


  // TODO: Toast Message (does not required BuildContext)
  static void showMessage(String message, Color myColour){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: myColour,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }


  // TODO: SnackBar Message (required BuildContext)
  static void showSuccessMessage(BuildContext context, String message){
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

}