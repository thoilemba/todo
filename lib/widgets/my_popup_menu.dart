import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/screens/addtask_screen.dart';
import 'package:todo/widgets/task_data.dart';

// TODO: This class provides the PopupMenuButton having Edit and Delete buttons

class MyPopupMenu extends StatelessWidget {

  final int index;
  final int id;

  MyPopupMenu({super.key, required this.id, required this.index});

  final taskData = TaskData();

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskData>(
        builder: (context, taskData, child) {
          return PopupMenuButton(
            itemBuilder: (context) {
              return [
                const PopupMenuItem(
                  value: 'edit',
                  child: Text('Edit'),
                ),
                const PopupMenuItem(
                  value: 'delete',
                  child: Text('Delete'),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 'edit') {
                // open edit page
                showModalBottomSheet(
                  isScrollControlled: true,
                  context: context,
                  builder: (context) =>
                      SingleChildScrollView(
                        child: Container(
                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: AddTaskScreen(true, value: taskData.tasks[index]['completed'], id: id, title: taskData.tasks[index]['title']),
                        ),
                      ),
                );
              } else if (value == 'delete') {
                // TODO: Confirmation on delete
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text("Delete Task"),
                      content: const Text("Do you want to delete this task?"),
                      actions: [
                        TextButton(
                          onPressed: () {
                            // delete the item
                            taskData.deleteData(context, id);
                            Navigator.pop(context);
                          },
                          child: const Text("Yes"),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text("No"),
                        ),
                      ],
                    );
                  },
                ); // showDialog
              } // end of else if
            }, // onSelected
          );
        }
    );
  }
}