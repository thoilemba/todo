import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/widgets/my_popup_menu.dart';
import 'package:todo/widgets/task_data.dart';

// TODO: This class provides each Task items that includes Checkbox, Task Title and Menu Button(Edit and Delete)

class TasksList extends StatelessWidget {

  const TasksList({super.key});

  @override
  Widget build(BuildContext context) {

    return Consumer<TaskData>(
      builder: (context, taskData, child) {
        // print('2 : ${taskData.hashCode}');
        return ListView.builder(
          // this padding is used to avoid the last task covered by the floating button
          padding: const EdgeInsets.only(bottom: 65),
          itemCount: taskData.taskCount,
          itemBuilder: (context, index) {
            final id = taskData.tasks[index]['id'];
            return Card(
              elevation: 2,
              child: ListTile(
                onLongPress: (){
                  showMenu(
                    context: context,
                    position: const RelativeRect.fromLTRB(100, 100, 0, 0),
                    items: [
                      const PopupMenuItem(
                        child: Text('Edit'),
                      ),
                      const PopupMenuItem(
                        child: Text('Delete'),
                      ),
                    ],
                  );
                },
                leading: Checkbox(
                  activeColor: Colors.lightBlueAccent,
                  value: taskData.tasks[index]['completed'],
                  onChanged: (value) => taskData.updateTick(value, index, id, taskData.tasks[index]['title']),
                ),
                title: Text("${taskData.tasks[index]['id']}.  ${taskData.tasks[index]['title']}",
                  style: const TextStyle(
                    // color: Colors.green,
                      fontSize: 16
                  ),
                ),
                trailing: MyPopupMenu(id: id, index: index),
              ),
            );
          },
        );
      }
    );
  }

  // TODO: SnackBar Message (required BuildContext)
  static void showSuccessMessage(BuildContext context, String message){
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

}




