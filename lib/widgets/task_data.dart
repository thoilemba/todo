import 'package:flutter/cupertino.dart';
import 'package:todo/network.dart';

// TODO: This class represents all the task items and all CRUD operations functionality by notifying each new changes

class TaskData extends ChangeNotifier {

  // Representing all the tasks items by a List
  List tasks  = [];

  // default constructor to get all the task items
  // when the screen loads for the first time
  TaskData(){
    getData();
  }

  int get taskCount {
    return tasks.length;
  }

  Future<dynamic> getData() async {
    final response = await NetworkHelper.fetchData();
    tasks = (response['data']);
    notifyListeners();
  }

  void addTask(String title) async {
    await NetworkHelper.postData(title);
    final response = await NetworkHelper.fetchData();
    tasks = (response['data']);
    notifyListeners();
  }

  void updateTick(bool? value, int index, int id, String title) async {

    await NetworkHelper.checkBoxUpdate(value, id, title);
    tasks[index]['completed'] = !tasks[index]['completed'];
    notifyListeners();

  }

  void updateTitle(bool? value, int id, String title) async {

    await NetworkHelper.update(value, id, title);
    final response = await NetworkHelper.fetchData();
    tasks = (response['data']);
    notifyListeners();

  }

  void deleteData(BuildContext context, int id) async {

    await NetworkHelper.deleteByID(context, id);

    final response = await NetworkHelper.fetchData();
    tasks = (response['data']);
    notifyListeners();
  }

}