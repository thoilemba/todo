import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/widgets/task_data.dart';

// TODO: This class provides the screen of adding new task and editing existing task
// TODO: Used by the FloatingActionButton and Edit button of PopupMenu

class AddTaskScreen extends StatelessWidget {

  // isEdited is used to indicate whether a task will be newly added or editing an existing one

  final bool isEdited;

  final bool value;
  final int id;
  final String title;

  // within curly braces are all optional parameters
  const AddTaskScreen(this.isEdited,  {super.key,this.value=false, this.id=0,
    this.title=''});

  @override
  Widget build(BuildContext context) {

    String? taskTitle;

    return Container(
      // this parent container is used to show
      // the rounded edges of the child container below
      color: const Color(0xff757575),
      child: Container(
        padding: const EdgeInsets.all(20),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              isEdited ? 'Edit Task' : 'Add Task',
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.lightBlueAccent,
                fontSize: 20,
              ),
            ),
            TextFormField(
              autofocus: true,
              textAlign: TextAlign.center,
              initialValue: isEdited ? title : '',
              onChanged: (newText) {
                taskTitle = newText;
              },
            ),
            TextButton(
              style: const ButtonStyle(
                backgroundColor:
                  MaterialStatePropertyAll<Color>(Colors.lightBlueAccent),
              ),
              onPressed: () {
                isEdited ?
                  Provider.of<TaskData>(context, listen: false).updateTitle(value, id, taskTitle!)
                : Provider.of<TaskData>(context, listen: false).addTask(taskTitle!);
                Navigator.pop(context);
              },
              child: Text(
                isEdited ? 'Update' : 'Add',
                style: const TextStyle(
                  color: Colors.white,
                ),
              )
            ),
          ],
        ),
      ),
    );
  }
}
