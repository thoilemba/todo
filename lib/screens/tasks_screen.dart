import 'package:flutter/material.dart';
import '../widgets/tasks_list.dart';
import 'addtask_screen.dart';

// TODO: This class provides the main screen that shows all the Tasks

class TasksScreen extends StatelessWidget {

  const TasksScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Todo List'),
      ),
      // Wrapping the FloatingActionButton inside a SizedBox to change its size
      floatingActionButton: SizedBox(
        width: 50,
        height: 50,
        child: FloatingActionButton(
          backgroundColor: Colors.lightBlueAccent,
          tooltip: 'Add task',
          onPressed: () {
            showModalBottomSheet(
              // By default, the BottomSheet will take up half the screen
              // For certain screen sizes, this may mean the Add button is obscured
              // Setting the isScrolledControlled property to true to make the modal take up the full screen
              isScrollControlled: true,
              context: context,
              builder: (context) => SingleChildScrollView(
                // To have the AddTaskScreen sit just above the keyboard, we can wrap it inside a SingleChildScrollView
                // which determines the padding at the bottom using a MediaQuery.
                child: Container(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                child: const AddTaskScreen(false),
                ),
              ),
            );
          },
          child: const Icon(Icons.add, size: 35),

        ),
      ),
      body:  const TasksList(),

    );
  }
}